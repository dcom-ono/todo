# **新人教育用サンプルアプリ** #



## **準備するもの** ##
* JRE 1.7
* Tomcat 7
* Eclipse（Tomcatプラグイン導入済み）
JRE、Tomcatの設定は[ここ](http://itmemo.net-luck.com/eclipse-java-servlet-environment/)を参考に行ってください。

## **開発環境構築**  ##
（１）課題ソースをダウンロードする。
![dev01.png](https://bitbucket.org/repo/RyAbG4/images/3049194296-dev01.png)
　
![dev02.png](https://bitbucket.org/repo/RyAbG4/images/2202181736-dev02.png)

（２）Eclipseを起動し、ダウンロードしたソースを既存プロジェクトとしてインポートする。

（３）インポートしたプロジェクトを右クリックし、[Tomcatプロジェクト]→[コンテキスト定義を更新]をクリックする。

（４）プロジェクトtodo→WEB-INF/src/application.propertiesを開き、以下の内容を変更する。
```
#!
database.path=[todoプロジェクト内にあるtodo.dbを絶対パスで指定]
```
（５）Tomcatを起動し、[http://localhost:8080/todo/list](http://localhost:8080/todo/list)にアクセスし、画面が表示されることを確認する。

## **アプリ概要** ##
[http://localhost:8080/todo/list](http://localhost:8080/todo/list)にアクセスすると、
タスク一覧画面が表示されます。

一覧画面上部の入力フィールドにタスクを入力し、作成ボタンで登録できます。

登録されたタスクは一覧に追加されます。

## **課題** ##

### **（１）編集機能を追加する** ###

![A01-01.png](https://bitbucket.org/repo/RyAbG4/images/4285370118-A01-01.png)

![A01-02.png](https://bitbucket.org/repo/RyAbG4/images/3954940794-A01-02.png)

***
### **（２）削除機能を追加する** ###

![A02-01.png](https://bitbucket.org/repo/RyAbG4/images/1464804152-A02-01.png)

***
### **（３）登録したタスクに優先順位を設定できるようにする** ###

![A03-01.png](https://bitbucket.org/repo/RyAbG4/images/1418093539-A03-01.png)

![A03-02.png](https://bitbucket.org/repo/RyAbG4/images/3888028282-A03-02.png)

***
### **（４）登録したタスクに期限を設定する** ###

![A04-01.png](https://bitbucket.org/repo/RyAbG4/images/1084120517-A04-01.png)

![A04-02.png](https://bitbucket.org/repo/RyAbG4/images/1619078242-A04-02.png)

***
### **（５）登録したタスクを検索条件をつけて絞り込む** ###

![A05-01.png](https://bitbucket.org/repo/RyAbG4/images/3349201866-A05-01.png)

![A05-02.png](https://bitbucket.org/repo/RyAbG4/images/1570453761-A05-02.png)

***
### **（６）タスク登録時に入力チェックを行う** ###

![A06-01.png](https://bitbucket.org/repo/RyAbG4/images/698326921-A06-01.png)

![A06-02.png](https://bitbucket.org/repo/RyAbG4/images/1925311724-A06-02.png)

***
### **（７）タスクを複数行入力できるようにする** ###

![A07-01.png](https://bitbucket.org/repo/RyAbG4/images/3302009986-A07-01.png)

![A07-02.png](https://bitbucket.org/repo/RyAbG4/images/1517148923-A07-02.png)