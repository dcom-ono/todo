package jp.co.dcom.todo.dto;

import java.io.Serializable;


/**
 * TODO情報の永続化クラス
 *
 * @author 2012DELL-20
 *
 */
public class Todo implements Serializable {

	/**
	 * ID（PK）
	 */
	private int id;

	/**
	 * TODO内容
	 */
	private String todo;

	/**
	 * IDを取得します。
	 * @return ID
	 */
	public int getId() {
		return id;
	}
	/**
	 * IDを設定します。
	 * @param id ID
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * TODO内容を取得します。
	 * @return TODO内容
	 */
	public String getTodo() {
		return todo;
	}
	/**
	 * TODO内容を設定します。
	 * @param todo TODO内容
	 */
	public void setTodo(String todo) {
		this.todo = todo;
	}
}
