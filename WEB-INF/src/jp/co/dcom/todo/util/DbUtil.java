package jp.co.dcom.todo.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ResourceBundle;

/**
 * データベース接続ユーティリティ
 *
 * @author 2012DELL-20
 *
 */
public class DbUtil {

	// 開発環境用
//	private static final String DB_URL = "jdbc:postgresql://localhost/tododb";
//	private static final String DB_USER = "postgres";
//	private static final String DB_PASS = "postgres";
//	private static final String DB_DRIVER = "org.postgresql.Driver";
	private static final String DB_URL = "jdbc:sqlite:{0}";
	private static final String DB_DRIVER = "org.sqlite.JDBC";

	// heroku用
//	private static final String DB_URL = "jdbc:postgresql://ec2-107-20-229-112.compute-1.amazonaws.com/dfpc589ddtgstb";
//	private static final String DB_USER = "ikzegjqdncbvzy";
//	private static final String DB_PASS = "GHaVoOmdccRMVMq8HFuopbh2sr";

	/**
	 * コネクション
	 */
	private Connection conn;

	/**
	 * ステートメント生成
	 *
	 * @param sql
	 *            クエリ
	 * @return ステートメント
	 * @throws SQLException
	 */
	public PreparedStatement createStatement(String sql) throws SQLException {
		return this.conn.prepareStatement(sql);
	}

	/**
	 * データベース接続します。
	 *
	 * @throws Exception
	 */
	public void connect() throws Exception {
		
		String dir = System.getProperty("user.dir");
	    System.out.println("ディレクトリ： " + dir);

		Class.forName(DB_DRIVER).newInstance();

//		this.conn = DriverManager.getConnection(DB_URL,DB_USER,DB_PASS);
		this.conn = DriverManager.getConnection(getConnectionUrl());

		// 自動コミットOFF
		conn.setAutoCommit(false);

	}

	/**
	 * データベース切断します。
	 *
	 * @throws SQLException
	 */
	public void disconnect() throws SQLException {
		if (this.conn != null) {
			this.conn.close();
		}
	}


	/**
	 * コミットを実行します。
	 *
	 * @throws SQLException
	 */
	public void commit() throws SQLException {
		this.conn.commit();
	}

	/**
	 * ロールバックを実行します。
	 *
	 * @throws SQLException
	 */
	public void rollback() throws SQLException {
		this.conn.rollback();
	}
	
	private String getConnectionUrl(){
		ResourceBundle bundle = ResourceBundle.getBundle("application");
		String filePath = bundle.getString("database.path");
		return MessageFormat.format(DB_URL, filePath);
	}
}
