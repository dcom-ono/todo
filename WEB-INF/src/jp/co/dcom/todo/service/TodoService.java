package jp.co.dcom.todo.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.dcom.todo.dto.Todo;
import jp.co.dcom.todo.util.DbUtil;

/**
 * TODO情報に関する業務ロジック
 *
 * @author 2012DELL-20
 *
 */
public class TodoService {

	/**
	 * TODO情報を全て取得し、リストで返却します。
	 * @return TODOリスト
	 */
	public List<Todo> findAll(){
		DbUtil db = new DbUtil();
		List<Todo> todoList = new ArrayList<Todo>();

		try {
			// DB接続
			db.connect();

			// SQL文生成
			PreparedStatement ps = db.createStatement("SELECT todo FROM todo");

			// クエリ実行
			ResultSet rs = ps.executeQuery();

			// ResultSetをListに格納
			while(rs.next()){
				Todo todo = new Todo();
				todo.setTodo(rs.getString("todo"));
				todoList.add(todo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				// DB切断
				db.disconnect();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return todoList;

	}

	/**
	 * TODOを新規登録します。
	 * @param todo TODO情報
	 */
	public void create(Todo todo){
		DbUtil db = new DbUtil();
		try {

			// DB接続
			db.connect();

			// SQL文生成
			PreparedStatement ps = db.createStatement("INSERT INTO todo (todo) values(?)");

			// パラメータ設定
			ps.setString(1, todo.getTodo());

			// クエリ実行
			ps.executeUpdate();

			// コミット
			db.commit();

		}catch (Exception e) {
			try {
				// ロールバック
				db.rollback();
			} catch (SQLException e1) {
			}
		}finally{
			try {
				// DB切断
				db.disconnect();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

	}
}
