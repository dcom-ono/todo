<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
    <title>TODO</title>
  </head>
  <body>
    <div class="container">
      <h1>TODO</h1>
      <form method="post" action="create" role="form" class="form-inline">
        <div class="form-group">
          <input type="text" name="todo" class="form-control">
          <input type="submit" value="作成" class="btn btn-success">
        </div>
      </form>
      <table class="table">
        <thead>
          <tr><th>内容</th></tr>
      	</thead>
        <tbody>
      <c:forEach var="todo" items="${ todoList}">
        <tr><td><c:out value="${todo.todo}"/></td></tr>
      </c:forEach>
        </tbody>
	  </table>
    </div><!-- /.container -->
  </body>
</html>